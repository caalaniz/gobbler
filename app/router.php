<?php
require_once(__DIR__ . "/start.php");

$controller = new GobblerController();
if (isset ($_GET['do'])) {
    $nav = $_GET['do'];
    switch ($nav) {
        case "index":
            break;
        case "login":
            $controller->login();
            break;
        case "logout":
            $controller->logout();
            break;
        case "gobble":
            break;
        case "register":
            $controller->register();
            break;
    }
} else {
    $controller->index();
}
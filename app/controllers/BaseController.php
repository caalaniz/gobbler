<?php
/**
 * Created by PhpStorm.
 * User: BlueDragon93
 * Date: 9/10/2015
 * Time: 11:13 PM
 */

require_once(__DIR__ . "/../start.php");

class BaseController
{
    public function __construct()
    {

    }

    public static function ShowView($view, $vars = array())
    {
        foreach ($vars as $k => $v) {
            ${$k} = $v;
        }
        include(VIEW_FOLDER . '/' . $view . ".php");
    }

    /**
     * Returns a clean string
     *
     * @param $data
     * @return string
     */
    public function Clean($data)
    {
        return htmlspecialchars($data);
    }

    /**
     * This function will only validates lenght and existance of fiields but it can later be extended to validate more
     * stuff
     * @param $data
     * @param $rules array
     * @return int
     */
    public function Validate($data, $rules)
    {
        foreach ($rules as $r => $argv) {

        }
        return 1;
    }

}
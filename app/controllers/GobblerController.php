<?php
/**
 * Created by PhpStorm.
 * User: BlueDragon93
 * Date: 9/10/2015
 * Time: 11:13 PM
 */

require_once(__DIR__ . "/../start.php");

class GobblerController extends BaseController
{

    public function __construct()
    {
        $this->data = [];
        $this->errors = null;
        parent::__construct();
    }

    public function index()
    {
        self::ShowView("index", $this->data);
    }

    public function login()
    {
        if (!isset($_POST['email'])) {
            $this->data["errors"]["MissingFieldError"]["email"] = "Field missing";
        }

        if (!isset($_POST['password'])) {
            $this->data["errors"]["MissingFieldError"]["password"] = "Field missing";
        }

        if (!isset($errors["MissingFieldError"])) {
            $user_name = $_POST['email'];
            $password = $_POST['password'];
            if (User::Guest()) {
                if (!User::Login($user_name, $password)) {
                    $this->data["errors"]["AuthenticationError"] = "Wrong credentials please try again";
                }
            }
        }

        self::ShowView("index", $this->data);

    }

    public function logout()
    {
        $user = User::Auth();
        if ($user) {
            if (!$user->logout()) {
                $this->data["errors"]["AuthenticationError"] = "You are not authenticated";
            }
        }
        self::ShowView("index", $this->data);
    }

    /**
     * showing how validating with a validator class will look like
     */
    public function Gobble($data)
    {
        if (User::Auth()) {
            $valid = new Validator($data, ['length' => 150]);
            $valid->validate();
            if ($valid->passed()) {
                $data = htmlspecialchars($data);
                User::Auth()->post($data);
            }
            $this->data["errors"]["ValidationError"] = $valid->getErrors();
        } else {
            $this->data["errors"]["AuthenticationError"] = "You are not authenticated";
            $this->data["errors"] = $this->errors;
        }
        self::ShowView("index", $this->data);
    }

    /**
     * Regular validation process...
     */
    public function register()
    {
        foreach (["name", "email", "password", "password_repeat"] as $field) {
            if (!isset($_POST[$field])) {
                $this->data["errors"]["FieldError"][$field] = "Field missing";
            } else {
                if ($field = "password") {
                    ${$field} = password_hash($_POST[$field], PASSWORD_DEFAULT);
                } else {
                    ${$field} = $_POST[$field];
                }
            }
        }
        if (!isset($errors["MissingFieldError"])) {
            if ($password == $password_repeat) {
                if (User::Guest()) {
                    try {
                        $user = User::Create(["user_name" => $name, "user_email" => $email, "password" => $password]);
                    } catch (\Exception $e) {

                    }
                }
            } else {
                $this->data["errors"]["FieldError"]["password"]= "Password and password repeat dont match";
            }

        }

        self::ShowView("index", $this->data);
    }
}
<?php
require_once(__DIR__ . "/../start.php");

class ValidationException extends Exception
{
}

class DuplicateKeyException extends Exception
{
    /**
     *  Parse the error message and gets the duplicate field's name.
     * @return string
     */
    public function getDuplicateKey()
    {
        $m = $this->getMessage();
        $m_explode = explode(" ", $m);
        $key = $m_explode[count($m_explode) - 1];
        return explode("'", $key)[1];
    }
}


class WrongArgumentType extends Exception
{
}
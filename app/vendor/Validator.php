<?php
require_once(__DIR__ . "/../start.php");

/**
 * Class Validator
 * This Class helps validate data
 * Rules must always be an associative array
 * ["function_name" => "rule" ]
 */
class Validator
{

    protected $data;

    /**
     * format ["function_name" => "rule" ]
     * @var array
     */
    protected $rules;

    /**
     * @var array
     */
    protected $errors;

    /**
     * @param $data string
     * @param $rules array
     */
    public function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
        $this->errors = [];
    }

    /**
     * @throws ValidationException
     */
    public function validate()
    {
        foreach ($this->rules as $rule => $param) {
            if (!function_exists($rule)) {
                $e = new ValidationException("Validation rule does not exist");
                var_dump($e);
                die();
            }
            try {
                $rule();
            } catch (\Exception $e) {
                if ($e instanceof ValidationException) {
                    $this->errors[$rule] = $e->getMessage();
                } else {
                    var_dump($e);
                    die();
                }
            }

        }
    }

    /**
     * @return bool
     */
    public function passed()
    {
        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Checks data being less or equal than a set length
     * @throws ValidationException
     * @throws WrongArgumentType
     */
    public function length()
    {
        if (!is_int($this->rules["length"])) {
            throw new WrongArgumentType("Length rule expects an integer," . gettype($this->rules["length"]) . " given");
        }
        if (!is_string($this->data)) {
            throw new WrongArgumentType("Length data expects a string," . gettype($this->data) . " given");
        }

        if (strlen($this->data) > $this->rules["length"]) {
            throw new ValidationException("Too long!!!");
        };
    }

    /**
     * Expects rule argument to be a string of format "table:column"
     * @throws Exception
     * @throws ValidationException
     */
    public function unique()
    {
        $dbh = BaseModel::connectionInit();
        $data = explode(':', $this->rules["unique"]);
        if (count($data)) {
            throw new Exception("Invalid syntax for unique validator, expected table:column");
        }
        foreach ($data as $d) {
            if (!ctype_alpha($d)) {
                throw new Exception("Not allowed character");
            }
        }
        $qry = "SELECT COUNT($data[0]) FROM $data[0] Where $data[1] = ?";
        $arg = [":id" => $this->data];
        $stm = $dbh->prepare($qry);
        $stm->execute($arg);

        if ($stm->fetchColumn() > 0) {
            throw new ValidationException($this->data . " It's a duplicate!");
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: BlueDragon93
 * Date: 9/10/2015
 * Time: 11:12 PM
 */

require_once(__DIR__ . "/../start.php");

class BaseModel
{
    protected static $primary_key = "id";
    protected static $table_name;
    protected static $visible = [];
    protected static $fields;

    public function __construct($data = NULL, $showAll = false)
    {
        static::$fields = $this->get_column_names();
        //Copy
        if ($data != NULL) {
            $keys = array_keys($data);
            $show = ($showAll || empty(static::$visible)) ? $keys : static::$visible;
            foreach (array_intersect($keys, $show) as $column) {
                $this->{$column} = $data[$column];
            }
        }

    }

    /**
     * @return PDO
     */
    public static function connectionInit()
    {
        $conn_settings = include(__DIR__ . "/../database/DbSettings.php");
        try {
            $dbh = new PDO('mysql:dbname=' . $conn_settings["db_database"] . ';host=' . $conn_settings["db_host"],
                $conn_settings["db_user"],
                $conn_settings["db_password"]);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        return $dbh;
    }

    /**
     * @param $dbh PDO
     * @param $values array
     * @return bool
     */
    public static function Create($values)
    {
        $fields = self::get_column_names();
        $arg = "";
        $columns = "";
        $update_values = array();
        self::separate_param($fields, $values, $update_values, $arg, $columns);
        //Create Query
        $qry = "INSERT INTO " . static::$table_name . "($columns)
                VALUES($arg)";
        $id = self::RawSql($qry, $update_values);
        return User::find($id);
    }

    /**
     * @return bool
     */
    public function delete()
    {
        $id = $this->fields[$this->primary_key];
        $qry = "DELETE FROM $this->table_name
        WHERE $this->primary_key = $id";
        self::RawSql($qry);
    }

    public static function findAll($key = false, $limit = false, $orderby = "DESC")
    {
        $k = self::get_column_names();
        $ka = array_search($key, $k);
        $order = $k[$ka];
        $limit = (is_int($limit)) ? $limit : false;
        $qry = "SELECT * FROM" . " " . static::$table_name . (($key) ? " ORDER BY $order DESC" : " ") .
            (($limit) ? " LIMIT $limit" : " ");
        $result = self::RawSql($qry);
        $collection = [];
        foreach ($result as $r) {
            $collection[] = new static($r);
        }
        return $collection;
    }

    /**
     * @param $key
     * @param null $field
     * @param bool|false $ShowAll
     * @return bool|User
     */
    public static function find($key, $field = NULL, $ShowAll = false)
    {
        if ($field == NULL) $field = static::$primary_key;
        $dbh = self::connectionInit();

        // Check if visible attribute is set.
        $columns = (is_array(self::$visible) && !empty(self::$visible)) && !$ShowAll ? implode(", ", self::$visible) : "*";

        //Query Example. Select * From users WHERE user_id = 1
        $qry = "SELECT $columns FROM " . static::$table_name . " WHERE $field = :key";
        $arg = [":key" => $key];

        $result = self::RawSql($qry, $arg);
        if (empty($result)) {
            return false;
        }
        return new Static($result[0], $ShowAll);
    }

    /**
     * @param $values array
     */
    public function update($values)
    {
        $update_values = array();
        $update_str = "";
        self::separate_param(static::$fields, $values, $update_values, $a = "", $c = "", $update_str);
        $pk = static::$primary_key;
        $qry = "UPDATE "
            . static::$table_name
            . " SET "
            . $update_str
            . " WHERE "
            . static::$primary_key . " = " . $this->{$pk};
        //var_dump($values);
        self::RawSql($qry, $update_values);
        $keys = array_keys($values);
        foreach (array_intersect($keys, static::$visible) as $column) {
            $this->{$column} = $values[$column];
        }
        return true;
    }

    /**
     * @param $qry
     * @param array $param
     * @return array|bool|string
     */
    protected static function RawSql($qry, $param = array())
    {
        $dbh = self::connectionInit();
        //Prepare query before binding
        $s = explode(" ", $qry);
        $stm = $dbh->prepare($qry);
        // Bind query with given parameters and execute the query with the given parameters

        try {
            $stm->execute($param);
        } catch (\PDOException $e) {
            if ($e->getCode() == 23000) {
                throw new DuplicateKeyException($e->getMessage(), $e->getCode(), $e);
            } else {
                throw $e;
            }
        }

        if (strtoupper($s[0]) == "INSERT") {
            return $dbh->lastInsertId();
        }
        if (strtoupper($s[0]) != "SELECT") {
            return true;
        }
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param $fields array
     * @param $values array
     * @param $arg string
     * @param $columns string
     * @param $update_values array
     * @param $update_arg string
     */
    private static function separate_param($fields, $values, &$update_values,
                                           &$arg = "", &$columns = "", &$update_arg = "")
    {
        $update_col = array_intersect_key($values, array_flip($fields));
        $update_values = array();
        $arg = "";
        $columns = "";
        $update_arg = "";
        foreach ($update_col as $k => $v) {
            $arg .= ",?";
            $update_values[] = $values[$k];
            $columns .= "," . $k;
            $update_arg .= "," . $k . '= ?';
        }
        // remove the first coma
        $arg = substr($arg, 1);
        $columns = substr($columns, 1);
        $update_arg = substr($update_arg, 1);
    }

    /**
     * @return array
     */
    protected static function get_column_names()
    {
        $dbh = self::connectionInit();
        $q = $dbh->prepare("DESCRIBE " . static::$table_name);
        $q->execute();
        $table_fields = $q->fetchAll(PDO::FETCH_COLUMN);
        return $table_fields;
    }
}
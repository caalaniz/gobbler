<?php
/**
 * Created by PhpStorm.
 * User: BlueDragon93
 * Date: 9/10/2015
 * Time: 11:12 PM
 */
require_once(__DIR__ . "/../start.php");

class User extends BaseModel
{
    protected static $primary_key = "user_id";
    protected static $table_name = "users";
    protected static $visible = ['user_id', 'user_name', 'user_email', 'user_creation_date'];

    /**
     * @return bool|User
     */
    public static function Auth()
    {
        if (isset($_SESSION['LOGGED']) && isset($_SESSION['USER'])) {
            return  $_SESSION['USER'];
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function Guest()
    {
        return !self::Auth();
    }

    /**
     * @return array|bool
     */
    public function followers()
    {
        $collection = [];
        $columns = is_array(self::$visible) ? implode(", ", self::$visible) : "*";
        $qry = "SELECT $columns FROM users WHERE user_id IN (
                  SELECT f.user_id
                  FROM users u
                    INNER JOIN user_follow f ON (f.follows_id = u.user_id)
                  WHERE u.user_id = :id
                )";
        $arg = [":id" => $this->user_id];
        $result = self::RawSql($qry, $arg);
        //Check if model was found
        if (empty($result)) {
            return false;
        }
        foreach ($result as $r) {
            $collection[] = new User($r);
        }
        return $collection;
    }

    /**
     * @return array|bool
     */
    public function following()
    {
        $collection = [];
        $columns = is_array(self::$visible) ? implode(", ", self::$visible) : "*";
        $qry = "SELECT $columns FROM users WHERE user_id IN (
                  SELECT follows_id
                  FROM users u
                  INNER JOIN user_follow f ON (f.user_id = u.user_id)
                  WHERE u.user_id = :id
                )";
        $arg = [":id" => $this->user_id];
        $result = self::RawSql($qry, $arg);
        //Check if model was found
        if (empty($result)) {
            return false;
        }
        foreach ($result as $r) {
            $collection[] = new User($r);
        }

        return $collection;

    }

    /**
     *
     */
    public function gobbles()
    {
        $qry = "SELECT * FROM " . Gobble::$table_name . "
        WHERE gobble_user = :id";
        $param = [":id" => $this->user_id];
        return $this->RawSql($qry, $param);
    }

    public function follow($user_id)
    {
        $sql = "INSERT INTO user_follow (user_id, follows_id) VALUES (" . $this->user_id . ",:follow_id)";
        $param = ["follow_id" => $user_id];
        return $this->RawSql($sql, $param);
    }


    public function un_follow($user_id)
    {
        $qry = "DELETE FROM user_follow WHERE follows_id = :follow_id AND user_id = :id ";
        //Prepare query before binding
        $param = ["follow_id" => $user_id, ":id" => $this->user_id];
        return $this->RawSql($qry, $param);
    }

    public function post($text)
    {
        $gobble = [
            'gobble_gobble' => htmlspecialchars($text, ENT_QUOTES, 'UTF-8'),
            'gobble_user'=> $this->user_id
        ];
        return Gobble::Create($gobble);
    }

    /**
     * @param $userEmail
     * @param $password
     * @return bool
     */
    public static function Login($userEmail, $password)
    {
        $user = User::find($userEmail, "user_email", true);
        if ($user != NULL) {
            if (password_verify($password, $user->user_password)) {
                $_SESSION['LOGGED'] = true;
                $_SESSION['USER'] = User::find($user->user_id);
                return true;
            }
            return false;
        }
    }

    /**
     * @return bool
     */
    public function logout()
    {
        session_destroy();
        return true;
    }
}
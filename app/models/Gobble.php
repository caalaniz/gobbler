<?php
/**
 * Created by PhpStorm.
 * User: BlueDragon93
 * Date: 9/10/2015
 * Time: 11:13 PM
 */

require_once(__DIR__ . "/../start.php");

class Gobble extends BaseModel
{
    protected static $table_name = "gobbles";

    public function __construct($data = NULL, $showAll = false)
    {
        parent::__construct($data, $showAll);
        if (isset($this->gobble_user)) {
            $this->gobble_user = User::find($this->gobble_user);
        }
    }

    public static function getLatest($limit = 20)
    {
        return self::findAll("gobble_date", $limit);

    }
}
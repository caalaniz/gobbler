<?php
/**
 * Created by PhpStorm.
 * User: BlueDragon93
 * Date: 9/10/2015
 * Time: 11:55 PM
 */
//Settings
require_once(__DIR__ . "/database/DbSettings.php");

//models
require_once(__DIR__ . "/models/BaseModel.php");
require_once(__DIR__ . "/models/Gobble.php");
require_once(__DIR__ . "/models/User.php");

//Controller
require_once(__DIR__ . "/controllers/BaseController.php");
require_once(__DIR__ . "/controllers/GobblerController.php");

//misc
require_once(__DIR__ . "/vendor/customexceptions.php");
require_once(__DIR__ . "/vendor/Validator.php");


//session start
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

//Define constants
define("DEBUG", TRUE);
//Local path for includes
define('VIEW_FOLDER', __DIR__ . "/views");
//generates a valid URI
define('PUBLIC_FOLDER', str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__ . "/public"));


function asset($asset_name)
{
    return PUBLIC_FOLDER . "/assets/" . $asset_name;
}
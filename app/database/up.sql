/*Create DB*/
DROP DATABASE IF EXISTS gobbler;
CREATE DATABASE IF NOT EXISTS gobbler;

/*Create tables*/
/*User table*/
CREATE TABLE IF NOT EXISTS gobbler.users (
  user_id            INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_name          VARCHAR(30)        NOT NULL,
  user_email         VARCHAR(50) UNIQUE NOT NULL,
  user_password      VARCHAR(60)        NOT NULL,
  user_creation_date TIMESTAMP       DEFAULT NOW()
);

/*Gobble table*/
CREATE TABLE IF NOT EXISTS gobbler.gobbles (
  gobble_id     INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  gobble_user   INT(6) UNSIGNED,
  gobble_gobble VARCHAR(141) NOT NULL,
  gobble_date   TIMESTAMP       DEFAULT NOW(),
  FOREIGN KEY (gobble_user) REFERENCES users (user_id)
);

/*Friends Table*/
CREATE TABLE IF NOT EXISTS gobbler.user_follow (
  user_id    INT(6) UNSIGNED NOT NULL,
  follows_id INT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (user_id, follows_id),
  FOREIGN KEY (user_id) REFERENCES users (user_id),
  FOREIGN KEY (follows_id) REFERENCES users (user_id)
);


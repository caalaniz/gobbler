<!DOCTYPE HTML>
<!--
	Big Picture by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>Big Picture by HTML5 UP</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="<?=asset(" js/ie/html5shiv.js");?>"></script><![endif]-->
    <link rel="stylesheet" href="<?= asset("css/main.css"); ?>"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?=asset(" css/ie8.css");?>" /><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?=asset(" css/ie9.css");?>" /><![endif]-->
</head>
<body>
<?php if (isset($errors)): ?>
    <?php foreach ($errors as $e): ?>
        <?= var_dump($e); ?>
    <?php endforeach; ?>
<?php endif; ?>
<!-- Header -->
<header id="header">

    <!-- Logo -->
    <h1 id="logo">Big Picture</h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="#intro">Intro</a></li>
            <li><a href="#one">What I Do</a></li>
            <li><a href="#two">Who I Am</a></li>
            <li><a href="#work">My Work</a></li>
            <li><a href="#contact">Contact</a></li>
        </ul>
    </nav>

</header>

<!-- Intro -->
<section id="intro" class="main style1 dark fullscreen">
    <div class="content container 75%">
        <header>
            <h2>Gobble.</h2>
        </header>
        <p>Welcome to <strong>Gobbler</strong> a little something to show what I can do.
            By <a href="http://whoiscarlos.com"> Carlos Alaniz</a>, built on <strong>PHP, MYSQL, JQUERY, HTML5 and
                CSS3</strong>,
        </p>
        <footer>
            <a href="#one" class="button style2 down">More</a>
        </footer>
    </div>
</section>

<!-- One -->
<section id="one" class="main style2 right dark fullscreen">
    <?php if (User::Guest()): ?>
        <div class="content box style2">
            <header>
                <h2>Login</h2>
            </header>
            <form method="post" action="?do=login">
                <div class="row 50%">
                    <div class="6u 12u(mobile)"><input type="email" name="email" placeholder="Email"/></div>
                    <div class="6u 12u(mobile)"><input type="password" name="password" placeholder="password"/></div>
                </div>
                <div class="row">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" value="Login"/></li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    <?php endif; ?>
    <a href="#two" class="button style2 down anchored">Next</a>
</section>

<!-- Two -->
<section id="two" class="main style2 left dark fullscreen">
    <?php if (User::Guest()): ?>
        <div class="content box style2">
            <header>
                <h2>Register</h2>
            </header>
            <form method="post" action="?do=register">
                <div class="row 50%">
                    <div class="6u 12u(mobile)"><input type="text" name="name" placeholder="Name"/></div>
                    <div class="6u 12u(mobile)"><input type="email" name="email" placeholder="Email"/></div>
                </div>
                <div class="row 50%">
                    <div class="6u 12u(mobile)"><input type="password" name="password" placeholder="password"/></div>
                    <div class="6u 12u(mobile)"><input type="password" name="password-repeat"
                                                       placeholder="Repeat password"/>
                    </div>
                </div>
                <div class="row">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" value="Register"/></li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    <?php else: ?>
        You logged in!

    <?php endif; ?>
    <a href="#work" class="button style2 down anchored">Next</a>
</section>

<section id="intro" class="main style1 dark fullscreen picture1">
    <div class="content container 75%">
        <header>
            <h2>Latest Gobbles.</h2>
        </header>

        <?php $g = Gobble::getLatest(20); ?>
        <?php for ($i = 0; $i <= count($g); $i++): ?>
            <div class="row">
                <?php for ($j = 0; $j < 3; $j++, $i++): ?>
                    <?php if (isset($g[$i])): ?>
                        <div class="4u 12u(mobile)" style="text-align: left">
                            <img clas="gobbler" style="float: left; margin-right: .5em"
                                 src="http://www.gravatar.com/avatar/<?= md5($g[$i]->gobble_user->user_email); ?>?s=120">
                            <strong><?= $g[$i]->gobble_user->user_name; ?></strong>
                            <p><?= $g[$i]->gobble_gobble; ?></p>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        <?php endfor; ?>
        <footer>
            <a href="#one" class="button style2 down">More</a>
        </footer>
    </div>
</section>
<!-- Contact -->
<section id="contact" class="main style3 secondary">
    <div class="content container">
        <header>
            <h2>Gobble away!!</h2>

        </header>
        <div class="box container 75%">

            <!-- Contact Form -->
            <form method="post" action="#">
                <div class="row 50%">
                    <div class="12u"><textarea name="message" placeholder="Gobble" rows="3"></textarea></div>
                </div>
                <div class="row">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" value="Gobble"/></li>
                        </ul>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>

<!-- Footer -->
<footer id="footer">

    <!-- Icons -->
    <ul class="actions">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
        <li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
    </ul>

    <!-- Menu -->
    <ul class="menu">
        <li>&copy; Untitled</li>
        <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
    </ul>

</footer>

<!-- Scripts -->
<script src="<?= asset("js/jquery.min.js"); ?>"></script>
<script src="<?= asset("js/jquery.poptrox.min.js"); ?>"></script>
<script src="<?= asset("js/jquery.scrolly.min.js"); ?>"></script>
<script src="<?= asset("js/jquery.scrollex.min.js"); ?>"></script>
<script src="<?= asset("js/skel.min.js"); ?>"></script>
<script src="<?= asset("js/util.js"); ?>"></script>
<!--[if lte IE 8]>
<script src="<?=asset(" js/ie/respond.min.js");?>"></script><![endif]-->
<script src="<?= asset("js/main.js"); ?>"></script>

</body>
</html>